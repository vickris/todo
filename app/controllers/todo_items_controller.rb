class TodoItemsController < ApplicationController
  before_action :find_todo
  before_action :todo_item, except: [:new, :create]
  
  def new
    @todo_item = @todo.todo_items.new
  end
  
  def create
    @todo_item = @todo.todo_items.create(todo_item_params)
    redirect_to @todo
    
  end
  
  def destroy
    @todo_item.destroy
    
    redirect_to :back
  end
  
  
  def complete
    @todo_item.update_attribute(:completed_at, Time.now)
    redirect_to @todo, notice: "Todo updated"
    
  end
  
  private
   def find_todo
     @todo = Todo.find(params[:todo_id])
   end
   
   def todo_item
     @todo_item = @todo.todo_items.find(params[:id])
   end
   
   def todo_item_params
     params.require(:todo_item).permit(:content)
   end
end
