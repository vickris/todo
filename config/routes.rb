Rails.application.routes.draw do
  
  get 'todo_items/create'

  root 'todos#index'
  resources :todos do
    resources :todo_items  do
      member do
        patch :complete
      end
    end
  end
  
end
